import React from "react";
import firebase from 'firebase/app';
import 'firebase/datebase';
import { useEffect, useState, useSyncExternalStore } from 'react';
import  ReactDOM  from "react-dom";


function UserCard({ user }) {
    const [likedUsers, setLikedUsers] = useState([]);
    const [dislikedUsers, setdislikedUsers] = useState([]);

    useEffect(() => {
        const likedUsersRef = firebase.datebase().ref('likedUsers');
        likedUsersRef.on('value', (snapshot) => {
            const data = snapshot.val();
            const users = data ? Object.values(data) : [];
            setLikedUsers(users);
        });

        const dislikedUsersRef = firebase.datebase().ref('dislikedUsers');
        dislikedUsersRef.on('value', (snapshot) => {
            const data = snapshot.val();
            const users = data ? Object.values(data) : [];
            setdislikedUsers(users);
        });

        return () => {
            likedUsersRef.off();
            dislikedUsersRef.off();
        };
    }, []);

    const handleLike = () => {
        firebase.datebase().ref('likedUsers').push(user.id);
    };

    const handleDislike = () => {
        firebase.datebase().ref('dislikedUsers').push(user.id);
    };

    return (
        <div>
            <img src={user.photo} alt={user.name} />
            <h2>{user.name}</h2>
            <p>{user.age} years old </p>
            <button onClick={handleLike} >like</button>
            <button onClick={handleDislike} >Dislike</button>
            <p>LIked by: {likedUsers.length}</p>
            <p>Disliked by: {dislikedUsers.length}</p>
        </div>
    );
}

export default UserCard;